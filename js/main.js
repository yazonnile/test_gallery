jQuery(function() {
	initGallery();
});

function initGallery() {
	jQuery('.gallery-holder').each(function(i) {
		var galleryInstance = new Gallery({
			holder: this,
			autorotation: !!(i%2)
		});
	});
}

function Gallery(opt) {
	this.options = jQuery.extend({
		holder: null,
		gallery: '.gallery',
		slides: '.slide',
		p_holder: '.parallax-elements',
		p_elements: '> *',

		btnPrev: '.prev',
		btnNext: '.next',
		btnPlayStop: '.play-stop',
		pagination: '.pagination',

		autorotation: false,
		switchTime: 2000,
		animSpeed: 750,

		activeClass: 'active'
	}, opt);
	this.init();
}

Gallery.prototype = {
	init: function() {
		this.getStructure();
		this.buildElements();
		this.buildPagination();
		this.refreshPager();
		this.attachEvents();
		this.autorotate();
	},
	getStructure: function() {
		this.holder = jQuery(this.options.holder).data('Gallery', this);
		this.gallery = this.holder.find(this.options.gallery);
		this.slides = this.gallery.find(this.options.slides);
		this.p_holder = this.holder.find(this.options.p_holder).css({width: this.slides.length + '00%'});
		this.p_elements = this.p_holder.find(this.options.p_elements);

		this.btnPrev = this.holder.find(this.options.btnPrev);
		this.btnNext = this.holder.find(this.options.btnNext);
		this.btnPlayStop = this.holder.find(this.options.btnPlayStop);
		this.pagination = this.holder.find(this.options.pagination);

		this.currentIndex = 0;
		this.autorotation = this.options.autorotation;
		this.isBusy = false;
	},
	buildElements: function() {
		jQuery(this.p_elements).each(function() {
			new ParallaxElement({
				element: this
			});
		});
	},
	buildPagination: function() {
		var pagerString = '';

		for (var i = 1, slidesLength = this.slides.length; i <= slidesLength; i++) {
			pagerString += '<li><a href="#">'+i+'</a></li>\t';
		}

		this.pagination.html(pagerString);
		this.pagerLinks = this.pagination.find('li');
	},
	attachEvents: function() {
		this.attachPagination();
		this.attachControls();

		jQuery(window).on('load resize orientationchange', jQuery.proxy(this.onResize, this));
	},
	attachPagination: function() {
		var self = this;

		this.pagerLinks.each(function(i) {
			var curPager = jQuery(this);
			(function(j) {
				curPager.on('click', function(e) {
					e.preventDefault();
					if (self.isBusy) return;
					self.currentIndex = j;
					self.numSlide(self.currentIndex);
				});
			}(i));
		});
	},
	attachControls: function() {
		this.btnPrev.on('click', jQuery.proxy(function(e) {
			e.preventDefault();
			this.prevSlide();
		}, this));
		this.btnNext.on('click', jQuery.proxy(function(e) {
			e.preventDefault();
			this.nextSlide();
		}, this));
		this.btnPlayStop.on('click', jQuery.proxy(function(e) {
			e.preventDefault();
			this.toggleAutorotation();
		}, this));
	},
	onResize: function() {
		clearTimeout(this.resizeTimer);
		this.resizeTimer = setTimeout(jQuery.proxy(function() {
			this.switchSlides(true);
			this.refreshElements();
		}, this), 10);
	},
	prevSlide: function() {
		if (this.isBusy) return;
		if (this.currentIndex === 0) this.currentIndex = this.slides.length - 1;
		else this.currentIndex--;

		this.switchSlides();
	},
	nextSlide: function() {
		if (this.isBusy) return;
		if (this.currentIndex === this.slides.length - 1) this.currentIndex = 0;
		else this.currentIndex++;

		this.switchSlides();
	},
	switchSlides: function(fromResize) {
		this.isBusy = true;
		this.refreshPager();
		this.stopAutorotation();

		if(fromResize) {
			this.gallery.css({
				left: -this.slides.eq(this.currentIndex).position().left
			});
			this.isBusy = false;
			this.autorotate();
			this.refreshElements();
		} else {
			this.gallery.animate({
				left: -this.slides.eq(this.currentIndex).position().left
			}, {
				duration: this.options.animSpeed,
				complete: jQuery.proxy(function() {
					this.isBusy = false;
					this.autorotate();
				}, this),
				step: jQuery.proxy(function(x) {
					this.refreshElements(x);
				}, this)
			});
		}
	},
	refreshPager: function() {
		this.pagerLinks.removeClass(this.options.activeClass).eq(this.currentIndex).addClass(this.options.activeClass);
	},
	refreshElements: function(x) {
		var offset = (x || this.gallery.position().left).toFixed();

		for (var i = 0, eleLength = this.p_elements.length; i < eleLength; i++) {
			this.p_elements.eq(i).data('ParallaxElement').refreshPosition(offset, this.p_holder.width());
		}
	},
	toggleAutorotation: function() {
		this.autorotation = !this.autorotation;
		this.autorotate();
	},
	startAutorotation: function() {
		clearTimeout(this.timer);
		this.timer = setTimeout(jQuery.proxy(function() {
			this.nextSlide();
			this.startAutorotation();
		}, this), this.options.switchTime);
	},
	stopAutorotation: function() {
		clearTimeout(this.timer);
	},
	autorotate: function() {
		this.btnPlayStop.toggleClass(this.options.activeClass, this.autorotation);
		this.autorotation ? this.startAutorotation() : this.stopAutorotation();
	}
}

function ParallaxElement(opt) {
	this.options = jQuery.extend({
		element: null
	}, opt);
	this.init();
}

ParallaxElement.prototype = {
	init: function() {
		var self = this;
		this.element = jQuery(this.options.element).data('ParallaxElement', this);
		this.data = jQuery.parseJSON(this.element.attr('title'));
		
		this.element.css({
			position: 'absolute',
			top: this.data.top,
			zIndex: this.data.z || 5
		}).removeAttr('title');
	},
	refreshPosition: function(offset, start) {
		this.element.css({
			left: offset/this.data.index + start*this.data.left/100
		});
	}
}